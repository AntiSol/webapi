<?php

/**
 * AntiSol's Web API Classes
 * (C)opyright Dale Magee, 2012-date('Y');
 * BSD 3-clause license
 */

/*
 * WebAPI handles command line params to set debugging options:
 */
$_ARG = array();
if (isset($argv)){
	foreach ($argv as $arg) {
		if (ereg('--([^=]+)(=(.*))?',$arg,$reg)) {
			if (!$reg[3]) $reg[3] = true;
			$_ARG[$reg[1]] = $reg[3];
		} elseif(ereg('-([a-zA-Z0-9])',$arg,$reg)) {
			$_ARG[$reg[1]] = 'true';
		}
	}
}

/**
 * True to dump data for all HTTP requests:
 * 	use --http_debug to set this
 * or you can define HTTP_DEBUG before including this file
 */
if (!defined("HTTP_DEBUG")) {
	$http_debug = isset($_ARG['http_debug']);
	define('HTTP_DEBUG',$http_debug);
}


/**
 * A Generic Web API Interface for doing HTTP requests
 * @author antisol
 *
 */
class WebAPI {
	/**
	 * Variables for doing HTTP basic authentication.
	 *	only Basic auth is supported in the base class.
	 */
	
	/**
	 * True to enable basic authentication
	 * @var Boolean
	 */
	public $use_auth = False;
	/**
	 * HTTP basic auth username
	 * @var string
	 */
	public $username = null;
	/**
	 * HTTP basic auth password
	 * @var string
	 */
	public $password = null;

	/**
	 * User Agent string, will be added to headers for all http requests.
	 * @var String
	 */
	public $user_agent = "AntiSol's WebAPI tool. By Dale Magee. Email: antisol@antisol.org";
	
	/*
	 * populated with response headers after each request
	 */ 
	public $response_headers = Array();

	/**
	 * Default headers to be sent in every request.
	 * 	each item should be a complete header, i.e "host: foo.bar", not "host" => "foo.bar"
	 * DO NOT include:
	 * 		user agent, use $this->user_agent 
	 * 		auth. use the bultins for auth ($this->auth_header(), $this->username, password, use_auth)
	 * 		content type.
	 * @var Array(header,header)
	 */
	public $default_headers = Array(
	);
	
	/*
	 * If this is true, webapi classes will throw an exception when a HTTP error occurs
	 * 	otherwise failed HTTP requests will simply return false. 
	 * HTTP errors will always be logged using error_log()
	 */
	public $throw_exceptions = false;
	
	/**
	 * Value to go in content type header when doing POST/DELETE (and maybe others)
	 * 
	 * To use a different content type for POST/DELETE requests, set this 
	 * 	value as appropriate, and point $this->encoder_func to the appropriate 
	 * 	function
	 * 
	 * @var string
	 */
	public $content_type = "application/x-www-form-urlencoded";
	
	/**
	 * Name of the function (must be a method of $this) to do encoding to match $content_type 
	 * @var string
	 */
	public $encoder_func = "form_urlencode";
	
	/**
	 * Changes the user agent string to pretend to be a browser
	 * 	pass either a fully-formed user agent string, or the key for one of the 
	 * 		known browsers in $browsers
	 *  e.g: $this->spoof_browser('firefox') or $this->spoof_browser('Wget/1.9.1')
	 *  
	 * @param string $browser
	 */
	public function spoof_browser($browser = 'firefox') {
		$browsers = Array(
			'firefox_ubuntu' => 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:38.0) Gecko/20100101 Firefox/38.0',
			'firefox_win' => 'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0',
			'chrome' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36',
			'opera' => 'Opera/9.80 (X11; Linux i686; Ubuntu/14.10) Presto/2.12.388 Version/12.16',
			'safari' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A',
			'curl' => 'curl/7.9.8 (i686-pc-linux-gnu) libcurl 7.9.8 (OpenSSL 0.9.6b) (ipv6 enabled)',
			'wget' => 'Wget/1.9.1',
		);
		$browsers['firefox'] = $browsers['firefox_ubuntu'];
		
		if (isset($browsers[$browser]))
			$this->user_agent = $browsers[$browser];
		else 
			$this->user_agent = $browser;
	}
	
	/**
	 * Generates and returns a Basic HTTP Authetication header
	 * @return string
	 */
	public function auth_header() {
		if (!$this->use_auth || !$this->username) return null;
		return 'Authorization: Basic ' .
			base64_encode("{$this->username}:{$this->password}");
	}

	/**
	 * Turns an Array of headers into a header string
	 * @param Array $headers
	 */
	public function build_headerstring($headers) {
		$ret = "";
		foreach ($headers as $h) {
			$ret .= "$h\r\n";
		}
		return $ret;
	}

	/**
	 * Turns an array of key=>value pairs, or a string, into a querystring
	 * @param mixed $items
	 */
	public function build_querystring($items) {
		$ret = "";
		if (is_array($items)) {
			foreach ($items as $k => $v)
				$ret .= ($ret?"&":"") . urlencode($k) . "=" . urlencode($v);
		} else $ret = strval($items);
		$ret = ($ret?"?":"") . $ret;
		return $ret;
	}
	
	public function content_type_header($type = "application/x-www-form-urlencoded") {
		return "Content-type: $type";
	}
	
	/**
	 * Function to do json encoding for POST/DELETE content
	 * this is here so that you can set encoder_func to json_encode 
	 * for JSON encoding
	 * @param any $data
	 * @return string
	 */
	public function json_encode($data) {
		return json_encode($data);
	}

	/**
	 * Function to do form urlencoding for POST/DELETE content. 
	 * This is the default encoder_func
	 * @param any $data
	 * @return string
	 */
	public function form_urlencode($data) {
		return http_build_query($data);
	}

	/**
	 * Performs an HTTP request.
	 *	This is the bare-bones method, you probably want http_get or http_post
	 *	unless you're doing something special
	 * @param string $url
	 * @param str|Array $querystring
	 * @param Array $headers
	 * @param GET|POST $method
	 * @param Array $postData
	 */
	public function http($url,$querystring = null,$headers = null, $method = "GET",$postData = null) {
		//$headers needs to be an array:
		if (!$headers) $headers = $this->default_headers;
		
		//build and add auth header if use_auth is true:
		if ($this->use_auth)
			$headers[] = $this->auth_header();

		//add a User Agent header (required by github):
		$headers[] = "User-Agent: " . $this->user_agent;
		
		//use key 'http' even if you send the request to https://...
		$options = array(
			'http' => array(
				'method'  => $method,
			)
		);

		if ($method == "POST" || $method == "DELETE" || $method == "PUT") {	//may need to add other methods here
			if ($postData) {
				/* Note: the default content type / encoding is form-urlencoded
				 * 	To change this, set $this->content_type to the correct value 
				 * 	for the content type header, and set $this->encoder_func to 
				 * 	the name of the encoder function (which must exist as a 
				 * 		method of this class, or the descendant)
				 * 	i.e for json encoding:
				 * 		$this->content_type = "application/json"
				 * 		$this->encoder_func = "json_encode"
				 * If you're using a subclass and you always want the same 
				 * encoding, you can just redeclare these vars in your subclass
				 */ 
				$headers[] = $this->content_type_header($this->content_type);
				$options['http']['content'] = $this->{$this->encoder_func}($postData);
			}
		}
		
		//(build headers last)
		$options['http']['header'] = $this->build_headerstring($headers);

		//handle querystrings:
		if ($querystring) {
			if (is_array($querystring)) $querystring = $this->build_querystring($querystring);
			if (substr($querystring,0,1) != "?") $querystring = "?" . $querystring;
			$url = $url . $querystring;
		}
		
		if (HTTP_DEBUG) {
			echo "HTTP $method: '$url' Options:\n";
			print_r($options);
		}

		$context  = stream_context_create($options);
		$result = null;
		
		$result = @file_get_contents($url, false, $context);
		
		if ($result === false) { //HTTP error
			$err = error_get_last();
			$msg = "WebAPI::http($method): Error retrieving '$url': " . $err['message'];
			
			//log it:
			error_log($msg);
			
			if (HTTP_DEBUG) {
				//output it:
				echo "$msg\n";
			}
			
			if ($this->throw_exceptions)
				//throw it
				throw new Exception($msg);
			
			return false;
		}
		
		$this->set_response_headers($http_response_header);
		
		if (HTTP_DEBUG) {
			echo "Response Headers:\n";
			print_r($this->response_headers);
		}
		
		return $result;
	}

	/**
	 * Parses $http_response_headers and populates $this->response_headers
	 */
	function set_response_headers($headers) {
		$parsed = Array();
		if (!$headers) return false;
		foreach ($headers as $header) {
			//handle response code (HTTP/1.1 <code> <response>) 
			$matches = Array();
			if (preg_match('|HTTP/1.1 (\d+) (.*)|',$header,$matches)) {
				$parsed['code'] = $matches[1];
				$parsed['response'] = $matches[2];
			} else {
				if (preg_match("/(.*?): (.*)/",$header,$matches))
					$parsed[$matches[1]] = $matches[2];
				else
					echo "could not parse response header: '$header'\n";
			}
			
		}
		
		$this->response_headers = $parsed;
	}
	
	/**
	 * A friendlier option than $this->http for doing most GET requests
	 * @param string 		$url	the URL
	 * @param Array|string 	$querystring	The querystring, either as a string 
	 * 			or an array of key/value pairs	 
	 */
	public function http_get($url,$querystring = null) {
		return $this->http($url,$this->build_querystring($querystring));
	}

	/**
	 * A friendlier option than $this->http for doing most POST requests
	 * @param string	$url	the URL
	 * @param array		$data	POST data
	 * @param array|string $querystring	the querystring 
	 */
	public function http_post($url,$data,$querystring = null) {
		return $this->http($url,$querystring,null,"POST",$data);
	}
}

