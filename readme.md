AntiSol's WebAPI library

Copyright Dale Magee, 2012-date('Y').

BSD 3-clause license.

This is a super-useful, rock-solid helper class that I've been using for years.

It provides an easy way to do HTTP requests that are more than a simple GET
(which can sometimes be as simple as using file_get_contents with a URL)

It can be used as-is for lots of things that are fairly standard HTTP, or
it can be easily extended to easily do advanced things, e.g for accessing
web APIs. I have used subclasses of WebAPI for Cloudflare, Gitlab, Github, 
and AffinityLive. But it's intended to be usable for any HTTP-based API.

# Features:
* Basic HTTP authentication
* Set HTTP request headers on a per-request basis, and/or set default
	request headers to do a bunch of requests.
* Access HTTP response headers
* Smart handling of querystrings (use an array of key/value pairs that 
	you can easily manipulate
* User agent spoofing - pretend to be firefox! Several built-in user agent
	strings and the ability to set your own
* Support for different encoding types for POST-ish requests. 
	form-urlencoded and JSON encoding built-in. Easily roll your own.
* Support for some of the more exotic request types (like PUT). 
	Easy to do others that aren't explicitly supported
* Easy GET and POST functions which will support the majority of scenarios

The library is not exhaustive, there are things it could do better, I tend
to add to it on an as-needed basis. Pull requests are very welcome!	
	
# HOWTOS:

## A simple GET request with basic authorisation and a querystring:

```php
<?php

$api = new WebAPI();

//basic auth:
$api->username = 'myusername';
$api->password = 'mypassword';
$api->use_auth = True;

$qs = Array(
	'some_value' => '1',
	'answer_to_ultimate_question' => 42,
);

$ret = $api->http_get('https://some-server.com/path/to/thing',$qs);

echo $ret;

```

## I'm totes firefox, honest!

```php
<?php

$api = new WebAPI();

$api->spoof_browser('firefox');

echo $api->http_get('https://whatsmyuseragent.com');

```

## POST some data with JSON encoding to a URL with a querystring, then examine response headers:

```php
<?php

$api = new WebAPI();

$api->content_type = "application/json";
$api->encoder_func = "json_encode";

$postdata = Array(
	"somevalue" => 1,
	"answer_to_ultimate_question" => 42,
);

$querystring = Array(
	"page" => 10,
	"apikey" => "blablabla",
);

$ret = $api->http_post("https://some-website.com/path/to/thing",$data,$querystring);

if ($api->response_headers['code'] != 200) {	
	/**
	 * 'code' is always populated with the response code, 
	 *	and 'response' is the response text.
	 * Note that $api->response_headers is populated after each request -  
	 *	it will only contain response headers from the most recent request
	 */
	echo "ERROR: Got invalid response code " . $api->response_headers['code'] . " " . $api->response_headers['response'];
}

```

## Send a request header:

```php
<?php

$api = new WebAPI();
$api->default_headers = Array(	
	"super_secret_key: password", //note that this is the whole header, not a key/value pair
)
$ret = $api->http_get("http://totally-secure.com/api/list_passwords");

```

## Do a PUT request:

```php

<?php

$api = new WebAPI();

$id = 42;

$data = Array{
	'some_setting' => True
}

$ret = $api->http('https://myapi.com/users/' . $id,null,null,"PUT",$data);

```

## Create an interface to Cloudflare's API as a subclass of WebAPI, then use it to get a list of domains on your account:

```php
<?php

class CloudflareAPIv4 extends WebAPI {
	
	public $email = null;
	public $apikey = null;
	
	public $debug = False;
	
	public $url = "https://api.cloudflare.com/client/v4/";
	public $user_agent = "My Cloudflare API tool. contact: myemail@mydomain.com";
	
	public $content_type = "application/json";
	public $encoder_func = "json_encode";
	
	//use the auth_header function to add auth headers
	public $use_auth = True;
	
	/**
	 * Generates the X-Auth-Key and X-Auth-Email headers needed for authentication
	 * @see WebAPI::auth_header()
	 */
	public function auth_header() {
		return "X-Auth-Email:" . $this->email ."\r\n" .
				"X-Auth-Key:" . $this->apikey;
		 
	}
	
	/**
	 * Perform a Cloudflare API request.
	 * @param string $url	Request URL. should not include the endpoint or 
	 * 							leading slash (i.e 'user', not 'http://bla/user')
	 * @param Array $params Key=>Value array for querystring
	 * @param Array|String $data	data to send (e.g in POST)
	 * @param strigg	$method		HTTP method to use (GET/POST/DELETE/etc)
	 * @param bool		$add_pagination false to not automatically add 'per_page=999' to the querystring if it's not provided
	 * @return Array|StdClass The result. Note that this strips away the "outer" cloudflare result, only returning the data
	 * 							(i.e $response->result). Will abort the script if the API request fails, 
	 * 							Showing the error message/code.
	 * 							Note also that Cloudflare list the maximum for the "per_page" parameter as 100. At the Time
	 * 							of writing, this does not seem to be enforced, but to protect against that eventuality, this
	 * 							function looks at the result_info section of the cloudflare response and calls itself 
	 * 							recursively if there's more than one page of results, merging all the results.array
	 * 							(short version: this may do multiple API requests and will return ALL results, not just one 
	 * 								page)
	 */
	public function api_request($url,$params = Array(),$postdata = Null,$method="GET",$add_pagination=true) {
		
		//default to 999 items per page (i.e disable paging) unless page size 
		//	is already specified
		if ($add_pagination && (!isset($params['per_page'])))
			$params['per_page'] = 999;
		
		if ($this->debug) {
			$pv="";
			if ($params) {
				foreach ($params as $k=>$v)
					$pv .= ($pv?", ":"") ."$k=$v" ;
			}
			error_log("\t - CloudFlare_v4::$method:$url($pv)");
		}
		
		$result = $this->http($this->url . $url,$params,Array(),$method,$postdata,true);
		if (!$result) 
			die("\n\nError in HTTP Request\n");
		
		$data = json_decode($result);
		
		if (!$data->success) {
			//there was an error. Die horribly.
			$msg = "Cloudflare API Error: ";
			foreach($data->errors as $e) {
				$msg .= $e['code'] . ": " . $e['message'] . " ";
			}
			die($msg);
		}
		
		/**
		 * A short aside for any developer who thinks that mandatory pagination 
		 *	is a good idea:
		 * 
		 * It's not. What you achieve by enforcing mandatory pagination on your 
		 *	API is more requests to your API,
		 * because people want the whole list, not just a part of it, so they 
		 *	have to hit your API multiple times to get that list. Each time I 
		 *	hit your API there's an overhead involved with that 
		 * (establishing a connection, sending my request, etc etc) 
		 * 
		 * I'm not saying pagination is a bad thing (though too often I'm 
		 *	annoyed by paging through 20 items at a time), what I'm saying is 
		 *	that MANDATORY pagination is a bad thing. Make it optional.
		 * 
		 */
		 
		//if there's more than one page of results, automatically fetch all pages:
		if (isset($data->result_info) && $data->result_info->total_pages > 0 && 
				($data->result_info->total_pages != $data->result_info->page)) {
			
			//there are more pages to fetch. Do that...
			$page = $data->result_info->page + 1;
			if ($this->debug) echo "\t - CloudFlare_v4::pagination: Retrieving page $page...";
			$params['page'] = $page;
			$params['per_page'] = $data->result_info->per_page;
			
			//call self recursively with the new page parameters: 
			$next = $this->api_request($url,$params,$postdata,$method,false);
			
			return array_merge($data->result,$next);
			
		}
		
		return $data->result;
		
	}
	
	/**
	 * Returns a bunch of domain objects for domains we manage
	 */
	function get_domain_list() {
		return $this->api_request("zones");
	}
	
}

//Do stuff with the cloudflare API:

$api = new CloudflareAPIv4();
$api->email = $your_email_address_loaded_from_config_file_not_in_your_repo;
$api->apikey = $your_api_key_loaded_from_config_file_not_in_your_repo;

$ret = $api->get_domain_list();

print_r($ret);

```

